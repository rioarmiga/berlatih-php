<?php
function tentukan_nilai($number)
{
  if ($number >= 85) {
  	echo "Sangat Baik";
  } 
  elseif ($number>=70 ) {
  	echo "Baik";
  }
  elseif ($number >= 60) {
  	echo "Cukup";
  }
  elseif ($number <60) {
  	echo "Kurang";
  }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo "<br>";
echo tentukan_nilai(76); //Baik
echo "<br>";
echo tentukan_nilai(67); //Cukup
echo "<br>";
echo tentukan_nilai(43); //Kurang
?>